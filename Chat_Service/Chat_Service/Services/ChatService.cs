﻿using Chat_Server.Protos;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat_Service.Services
{
    
        public class ChatService : Chat.ChatBase
        {
            private readonly ChatRoom _chatRoom;

            public ChatService(ChatRoom chatRoom)
            {
                _chatRoom = chatRoom;
            }

            public override Task<NicknameReply> ConnectToChat(Nickname request, ServerCallContext context)
            {
                if (_chatRoom.ConnectedUsers.Contains(request.Nickname_))
                    return Task.FromResult(new NicknameReply { IsNicknameOk = false });
                else
                {
                    _chatRoom.AllMessages.Add(new Data()
                    {
                        Content = new Content() { Content_ = $"{request.Nickname_} has connected." },
                        Nickname = new Nickname() { Nickname_ = "" }
                    });
                    _chatRoom.ConnectedUsers.Add(request.Nickname_);
                    Console.WriteLine($"\n\n{request.Nickname_} has connected.\n\n");

                    return Task.FromResult(new NicknameReply() { IsNicknameOk = true });
                }
            }
            public override Task<MultipleNicknames> GetAllConnectedUsers(empty request, ServerCallContext context)
            {
                var itemsTransformed = _chatRoom.ConnectedUsers
                    .Select(item => new Nickname()
                    {
                        Nickname_ = item
                    }).ToList();
                var response = new MultipleNicknames();
                response.Nicknames.AddRange(itemsTransformed);
                return Task.FromResult(response);
            }

            public override Task<MultipleMessages> GetNewMessages(empty request, ServerCallContext context)
            {
                var itemsTransformed = _chatRoom.AllMessages
                    .Select(item => new Data()
                    {
                        Nickname = new Nickname()
                        {
                            Nickname_ = item.Nickname.Nickname_
                        },
                        Content = new Content()
                        {
                            Content_ = item.Content.Content_
                        }

                    });
                var response = new MultipleMessages();
                response.Datas.AddRange(itemsTransformed);
                return Task.FromResult(response);
            }

            public override Task<empty> SendMessage(Data request, ServerCallContext context)
            {
                _chatRoom.AllMessages.Add(request);
                return Task.FromResult(new empty());
            }
            public override Task<empty> SignOut(Nickname request, ServerCallContext context)
            {
                _chatRoom.ConnectedUsers.Remove(request.Nickname_);
                _chatRoom.AllMessages.Add(new Data()
                {
                    Content = new Content() { Content_ = $"{request.Nickname_} has left the room." },
                    Nickname = new Nickname() { Nickname_ = "" }
                });

                Console.WriteLine($"\n\n{request.Nickname_} has disconnected.\n\n");
                return Task.FromResult(new empty());
            }

        }
    
}
