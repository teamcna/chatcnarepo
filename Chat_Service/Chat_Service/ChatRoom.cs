﻿using Chat_Server.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chat_Service
{
    public class ChatRoom
    {
        private List<string> connectedUsers;


        public List<string> ConnectedUsers
        {
            get { return connectedUsers; }
            set { connectedUsers = value; }
        }

        private List<Data> allMessages;

        public List<Data> AllMessages
        {
            get { return allMessages; }
            set { allMessages = value; }
        }


        public ChatRoom()
        {
            connectedUsers = new List<string>();
            allMessages = new List<Data>();
        }

    }
}
