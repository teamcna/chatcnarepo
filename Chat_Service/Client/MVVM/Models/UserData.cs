﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Client.MVVM.Models
{
    class UserData
    {
        private static ObservableCollection<string> nickname = new ObservableCollection<string>();

        public static ObservableCollection<string> MyNickname
        {
            get { return nickname; }
            set { nickname = value; }
        }
    }
}
