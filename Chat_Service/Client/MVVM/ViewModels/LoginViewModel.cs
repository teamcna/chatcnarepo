﻿using Client.Core;
using Client.MVVM.Models;
using Client.MVVM.Views;
using ClientLibrary;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Client.MVVM.ViewModels
{
    class LoginViewModel : ObservableObject
    {
        public LoginViewModel()
        {
            ExitCommand = new RelayCommand(o =>
            {
                Environment.Exit(0);
            });
            LoginCommand = new RelayCommand(async o =>
            {
                if (nickname != "")
                {
                    await CallGrpcService();

                    var auto = (LoginView)Application.Current.Windows[0];

                    auto.Visibility = Visibility.Collapsed;

                }

                else MessageBox.Show("Nickname can't be empty.");
            });
        }
        private async Task CallGrpcService()
        {
            var Client = new GrpcServiceProvider().GetChatClient();
            var reply = await Client.ConnectToChatAsync(
                              new ChatProtos.Nickname { Nickname_ = nickname });
            if (reply.IsNicknameOk == false)
                MessageBox.Show("Nickname is already in use. Please choose another one.");
            else
            {

                UserData.MyNickname.Add(nickname);
                ChatView chat = new ChatView();
                chat.Show();
            }
            nickname = "";
        }

        private string nickname;

        public string Nickname
        {
            get { return nickname; }
            set
            {
                nickname = value;
                OnPropertyChange();
            }
        }

        private RelayCommand exitCommand;

        public RelayCommand ExitCommand
        {
            get { return exitCommand; }
            set
            {
                exitCommand = value;
                OnPropertyChange();
            }
        }
        private RelayCommand loginCommand;

        public RelayCommand LoginCommand
        {
            get { return loginCommand; }
            set
            {
                loginCommand = value;
                OnPropertyChange();
            }
        }



    }
}

