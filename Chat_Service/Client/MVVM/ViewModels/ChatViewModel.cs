﻿using ChatProtos;
using Client.Core;
using Client.MVVM.Models;
using ClientLibrary;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using static ChatProtos.Chat;

namespace Client.MVVM.ViewModels
{
    class ChatViewModel : ObservableObject
    {
        private string myNickname;

        public string MyNickname
        {
            get { return myNickname; }
            set { myNickname = value; }
        }

        private string toSendText;

        public string ToSendText
        {
            get { return toSendText; }
            set
            {
                toSendText = value;
                OnPropertyChange();
            }
        }

        private RelayCommand sendCommand;

        public RelayCommand SendCommand
        {
            get { return sendCommand; }
            set { sendCommand = value; }
        }


        private RelayCommandGeneric<Window> onExit;

        public RelayCommandGeneric<Window> OnExit
        {
            get { return onExit; }
            private set { onExit = value; }
        }




        private ObservableCollection<string> connectedUsers;

        public ObservableCollection<string> ConnectedUsers
        {
            get { return connectedUsers; }
            set
            {
                connectedUsers = value;
                OnPropertyChange();
            }
        }

        private ObservableCollection<Data> messages;

        public ObservableCollection<Data> Messages
        {
            get { return messages; }
            set
            {
                messages = value;
                OnPropertyChange();
            }
        }



        private ChatClient client;

        private DispatcherTimer timer;


        public ChatViewModel()
        {

            myNickname = UserData.MyNickname[UserData.MyNickname.Count - 1];
            ToSendText = "";
            client = new GrpcServiceProvider().GetChatClient();
            connectedUsers = new ObservableCollection<string>();
            messages = new ObservableCollection<Data>();
            SendCommand = new RelayCommand(o =>
            {
                client.SendMessage(new Data()
                {
                    Nickname = new Nickname()
                    {
                        Nickname_ = myNickname
                    },

                    Content = new Content()
                    {
                        Content_ = ToSendText
                    }

                });
                ToSendText = "";
            });
            OnExit = new RelayCommandGeneric<Window>(CloseGracefully);
            UpdateInfo();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += new EventHandler(TikTok);
            timer.Start();
        }
        private void CloseGracefully(Window window)
        {
            client.SignOut(new Nickname
            {
                Nickname_ = myNickname
            });
            timer.Stop();
            window.Close();
            Environment.Exit(0);
        }
        private void TikTok(object sender, EventArgs e)
        {
            UpdateInfo();
        }

        private void UpdateInfo()
        {

            UpdateConnectedUsers();
            UpdateMessages();
        }
        private void UpdateConnectedUsers()
        {
            var response = client.GetAllConnectedUsers(new empty());
            connectedUsers.Clear();
            foreach (Nickname n in response.Nicknames)
            {
                connectedUsers.Add(n.Nickname_);
            }
        }
        private void UpdateMessages()
        {
            var mess = client.GetNewMessages(new empty());
            foreach (Data d in mess.Datas)
            {
                //MessageViewModel message = new MessageViewModel();
                // message.Nickname = d.Nickname.Nickname_;
                // message.Text = parseTextToStyledText(d.Content.Content_);
                //  if (!messages.Contains(message))
                //  messages.Add(message);
                if (!messages.Contains(d))
                    messages.Add(d);
            }
        }

        private FormattedText ParseTextToStyledText(string message)
        {
            int index = 0;
            FormattedText newMessage = new FormattedText(
                              message,
                              CultureInfo.GetCultureInfo("en-us"),
                              FlowDirection.LeftToRight,
                              new Typeface("Garamond"),
                              15,
                              Brushes.Black);
            while (index < message.Length)
            {
                switch (message[index])
                {
                    case '_': //italic
                        {
                            if (message[index - 1] != ' ')
                                break;
                            int contor = 1;
                            while (message[index + contor] != '_')
                            {
                                contor++;
                                if (index + contor == message.Length)
                                    break;
                            }
                            if (message[index + contor + 1] != ' ')
                                break;
                            newMessage.SetFontStyle(FontStyles.Italic, index, contor);
                            break;
                        }
                    case '*': //bold
                        {
                            if (message[index - 1] != ' ')
                                break;
                            int contor = 1;
                            while (message[index + contor] != '*')
                            {
                                contor++;
                                if (index + contor == message.Length)
                                    break;
                            }
                            if (message[index + contor + 1] != ' ')
                                break;
                            newMessage.SetFontWeight(FontWeights.Bold, index, contor);
                            break;
                        }
                    case '~': //strike-through
                        {
                            if (message[index - 1] != ' ')
                                break;
                            int contor = 1;
                            while (message[index + contor] != '~')
                            {
                                contor++;
                                if (index + contor == message.Length)
                                    break;
                            }
                            if (message[index + contor + 1] != ' ')
                                break;
                            newMessage.SetTextDecorations(TextDecorations.Strikethrough, index, contor);
                            break;
                        }
                    case '`': //underlined
                        {
                            if (message[index - 1] != ' ')
                                break;
                            int contor = 1;
                            while (message[index + contor] != '~')
                            {
                                contor++;
                                if (index + contor == message.Length)
                                    break;
                            }
                            if (message[index + contor + 1] != ' ')
                                break;
                            newMessage.SetTextDecorations(TextDecorations.Underline, index, contor);
                            break;
                        }
                    case '\0':
                        {
                            break;
                        }
                }
                index++;
            }
            return newMessage;
        }
    }
}