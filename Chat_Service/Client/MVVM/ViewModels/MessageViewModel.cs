﻿using Client.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;

namespace Client.MVVM.ViewModels
{
    class MessageViewModel : ObservableObject
    {
        private string nickname;

        public string Nickname
        {
            get { return nickname; }
            set
            {
                nickname = value;
                OnPropertyChange();
            }
        }

        private FormattedText text;

        public FormattedText Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChange();
            }
        }
    }
}
